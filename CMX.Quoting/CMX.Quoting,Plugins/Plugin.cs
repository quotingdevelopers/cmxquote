﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.Globalization;

namespace CMX.Quoting.Plugins
{
    public class PluginBase : Plugin
    {
        public PluginBase() : base(typeof(PluginBase))
        {
            // Test of Logging - Post-Sync Create Account
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "account", new Action<LocalPluginContext>(Test.CreateAccount)));
        }
    }
}
