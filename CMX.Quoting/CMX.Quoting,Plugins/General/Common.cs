﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Newtonsoft;

namespace CMX.Quoting.Plugins
{
    public static class Core
    {
        /// <summary>
        /// Log information to cmx_log entity
        /// </summary>
        /// <param name="info">Log information (Object)</param>
        /// <param name="severity">Log severity (Int32) [0 - Debug, 1 - Info, 2 - Warning, 3 - Error, 4 - Critical] - Default Debug</param>
        /// <param name="name">Log name (String) [Log Name + Base.ErrorMessage] - Default Log</param>
        public static void Log(Plugin.LocalPluginContext context, Object info, Int32 severity = 0, String name = "Log")
        {
            string currentSeverityText = GetSetting(context, "LogSeverityLevel");
            int currentSeverity = Convert.ToInt32((currentSeverityText == "") ? "0" : currentSeverityText);
            if (severity >= currentSeverity)
            {

                Entity debugRecord = new Entity
                {
                    LogicalName = "cmx_log",
                };

                debugRecord.Attributes.Add("cmx_name", name);

                String toLog = Newtonsoft.Json.JsonConvert.SerializeObject(info);
                Boolean isBig = toLog.Length > 9500;

                Entity currentInvokeUser = Core.Data.GetRecord<Entity>(context, new EntityReference("systemuser", context.PluginExecutionContext.InitiatingUserId), new ColumnSet("domainname"));
                string currentInvokeUserLogin = currentInvokeUser.GetAttributeValue<string>("domainname");

                debugRecord.Attributes.Add("cmx_log", (isBig) ? toLog.Substring(0, 9500) : toLog);
                debugRecord.Attributes.Add("cmx_depth", context.PluginExecutionContext.Depth);
                debugRecord.Attributes.Add("cmx_messagename", context.PluginExecutionContext.MessageName);
                debugRecord.Attributes.Add("cmx_mode", new OptionSetValue(context.PluginExecutionContext.Mode));
                debugRecord.Attributes.Add("cmx_stage", new OptionSetValue(context.PluginExecutionContext.Stage));
                debugRecord.Attributes.Add("cmx_severity", new OptionSetValue(severity));
                debugRecord.Attributes.Add("cmx_primaryentityname", context.PluginExecutionContext.PrimaryEntityName);
                debugRecord.Attributes.Add("cmx_primaryentityid", context.PluginExecutionContext.PrimaryEntityId.ToString());
                debugRecord.Attributes.Add("cmx_user", currentInvokeUserLogin);
                Guid logId = context.OrganizationService.Create(debugRecord);
                if (isBig)
                {
                    Entity setupAnnotation = new Entity("annotation");
                    setupAnnotation.Attributes.Add("subject", name);
                    setupAnnotation.Attributes.Add("filename", "log.txt");
                    setupAnnotation.Attributes.Add("documentbody", Convert.ToBase64String(new UTF8Encoding().GetBytes(toLog)));
                    setupAnnotation.Attributes.Add("mimetype", "text/plain");
                    setupAnnotation.Attributes.Add("objectid", new EntityReference("cmx_log", logId));
                    context.OrganizationService.Create(setupAnnotation);
                };
            }
        }

        public static String GetSetting(Plugin.LocalPluginContext context, String name)
        {
            string value = "";
            try
            {
                QueryExpression settingQuery = new QueryExpression
                {
                    NoLock = true,
                    EntityName = "cmx_setting",
                    ColumnSet = new ColumnSet("cmx_value"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("cmx_name", ConditionOperator.Equal, name)
                        }
                    }
                };

                DataCollection<Entity> settings = context.OrganizationService.RetrieveMultiple(settingQuery).Entities;

                foreach (Entity setting in settings)
                {
                    value = setting.GetAttributeValue<string>("cmx_value");
                }
            }
            catch (Exception e)
            {
                Log(context, e, 3, "GetSetting");
            }
            return value;
        }

        public static class Data
        {
            public static T GetRecord<T>(Plugin.LocalPluginContext context, EntityReference reference, Boolean isActivity = false) where T : Entity
            {
                if (reference != null)
                {
                    return GetRecord<T>(context, reference.LogicalName, reference.Id, new ColumnSet(true), isActivity);
                }
                else
                {
                    return null;
                }
            }

            public static T GetRecord<T>(Plugin.LocalPluginContext context, EntityReference reference, ColumnSet columnSet, Boolean isActivity = false) where T : Entity
            {
                if (reference != null)
                {
                    return GetRecord<T>(context, reference.LogicalName, reference.Id, columnSet, isActivity);
                }
                else
                {
                    return null;
                }
            }

            public static T GetRecord<T>(Plugin.LocalPluginContext context, String entityName, Guid recordGUID, ColumnSet columnSet, Boolean isActivity = false) where T : Entity
            {
                Entity returnRecord = null;
                try
                {
                    QueryExpression entitiesQuery = new QueryExpression
                    {
                        NoLock = true,
                        EntityName = entityName,
                        ColumnSet = columnSet,
                        Criteria = new FilterExpression(LogicalOperator.And)
                        {
                            Conditions =
                                {
                                    new ConditionExpression(isActivity ? "activityid" : entityName + "id", ConditionOperator.Equal, recordGUID)
                                }
                        }
                    };

                    DataCollection<Entity> entities = context.OrganizationService.RetrieveMultiple(entitiesQuery).Entities;

                    foreach (Entity entity in entities)
                    {
                        returnRecord = entity;
                    }

                }

                catch (Exception e)
                {
                    Log(context, e, 3, "GetRecord");
                }
                return returnRecord != null ? returnRecord.ToEntity<T>() : null;
            }
        }
    }

    /// <summary>
    /// Base class for Plugin Context
    /// </summary>
}