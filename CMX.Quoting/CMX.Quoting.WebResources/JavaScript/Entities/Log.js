﻿Log = {
    OpenRecordEnable: function () {
        try {
            return Xrm.Page.getAttribute('cmx_primaryentityname').getValue() != '' && Xrm.Page.getAttribute('cmx_primaryentityname').getValue() != null && Xrm.Page.getAttribute('cmx_primaryentityid').getValue() != '' && Xrm.Page.getAttribute('cmx_primaryentityid').getValue() != null;
        } catch (e) {
            alert("OpenRecordEnable - " + e.message);
        }
    },

    OpenRecord: function () {
        try {
            var windowOptions = {
                openInNewWindow: true
            };
            if (Xrm.Page.getAttribute('cmx_formid').getValue() != '' && Xrm.Page.getAttribute('cmx_formid').getValue() != null) {
                var parameters = {};
                parameters.formid = Xrm.Page.getAttribute('cmx_formid').getValue();
                Xrm.Utility.openEntityForm(Xrm.Page.getAttribute('cmx_primaryentityname').getValue(), Xrm.Page.getAttribute('cmx_primaryentityid').getValue(), parameters, windowOptions);
            } else {
                Xrm.Utility.openEntityForm(Xrm.Page.getAttribute('cmx_primaryentityname').getValue(), Xrm.Page.getAttribute('cmx_primaryentityid').getValue(), null, windowOptions);
            }
        } catch (e) {
            alert("OpenRecord - " + e.message);
        }
    }
};