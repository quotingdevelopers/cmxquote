﻿Cemex = Cemex || {};

Cemex.Common = Cemex.Common || {};

Cemex.Common.WebAPI = {
    version: window.parent.APPLICATION_VERSION,

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A 
    // Related function(s): N/A
    // Req. ID: N/A
    RetrieveMultiple: function (query, callback) {
        /// <summary>This function retrieves collection of records using WebAPI with a query and callback as parameters.</summary>  
        /// <param name="query" type="Object">
        /// <par/>queryURL: WebAPI URL behind API version
        /// <para/>--- Without queryURL ---
        /// <para/>entitySet: Entity Set Name
        /// <para/>fields: ColumnSet (lookup = _lookupname_value)
        /// <para/>filter: Whole text of filter, see MSDN
        /// <para/>orderby: Ordering
        /// </param>  
        /// <param name="callback" type="Function">Function triggered after the data returns. If this parameter is null, the function is synchronnous.</param>  
        /// <returns type="Object{count: records count, value: array of Entities}"></returns> 
        try {
            var queryURL = "";
            var varSign = "?";
            if (query.hasOwnProperty("queryURL")) {
                queryURL = query.queryURL;
            } else {
                queryURL = query.entitySet;

                if (query.hasOwnProperty("fields")) {
                    queryURL += varSign + "$select=" + query.fields;
                    varSign = "&";
                }
                if (query.hasOwnProperty("filter")) {
                    queryURL += varSign + "$filter=" + query.filter;
                    varSign = "&";
                }
                if (query.hasOwnProperty("orderby")) {
                    queryURL += varSign + "$orderby=" + query.orderby;
                    varSign = "&";
                }
                queryURL += varSign + "$count=true";
            }
            var req = new XMLHttpRequest();

            if (callback === undefined) {
                req.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + queryURL, false);
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.send();
                if (req.status === 200) {
                    var resp1 = req.response.replace(/Microsoft.Dynamics.CRM.lookuplogicalname/gmi, "logicalname");
                    var resp2 = resp1.replace(/Microsoft.Dynamics.CRM.associatednavigationproperty/gmi, "property");
                    var resp3 = resp2.replace(/OData.Community.Display.V1.FormattedValue/gmi, "formated"); //_value@Microsoft.Dynamics.CRM.lookuplogicalname
                    var results = JSON.parse(resp3);
                    return { value: results.value, count: results["@odata.count"] };
                } else {
                    var error = JSON.parse(req.response).error;
                    alert("_RetrieveMultiple: " + error.message);
                }
            } else {
                req.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + queryURL, true);
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.onreadystatechange = function () {
                    try {
                        if (this.readyState === 4) {
                            req.onreadystatechange = null;
                            if (this.status === 200) {
                                var resp1 = this.response.replace(/Microsoft.Dynamics.CRM.lookuplogicalname/gmi, "logicalname");
                                var resp2 = resp1.replace(/Microsoft.Dynamics.CRM.associatednavigationproperty/gmi, "property");
                                var resp3 = resp2.replace(/OData.Community.Display.V1.FormattedValue/gmi, "formated"); //_value@Microsoft.Dynamics.CRM.lookuplogicalname
                                var results = JSON.parse(resp3);
                                callback(results.value, results["@odata.count"]);
                            }
                            else {
                                var error = JSON.parse(req.response).error;
                                alert("_RetrieveMultiple: " + error.message);
                            }
                        }
                    } catch (e) {
                        alert("_RetrieveMultiple: " + queryURL + " - " + e.message);
                    }
                };
                req.send();
            }
        } catch (e) {
            alert("RetrieveMultiple: " + e.message);
        }

    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    Retrieve: function (entitySet, id, select) {
        /// <summary>This function retrieves a record using WebAPI with a query and callback as parameters.</summary>  
        /// <param name="entitySet" type="String">Entity Set Name</param> 
        /// <param name="id" type="String">Entity GUID</param> 
        /// <param name="select" type="String">Select</param> 
        /// <returns type="Object">Object - Entity</returns> 
        try {
            var queryURL = "";
            if (select) {
                queryURL = entitySet + "(" + id.replace('{', '').replace('}', '') + ")?$select=" + select;
            } else {
                queryURL = entitySet + "(" + id.replace('{', '').replace('}', '') + ")";
            }

            var req = new XMLHttpRequest();
            req.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + queryURL, false);
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("Prefer", "odata.include-annotations=*");
            req.send();
            if (req.status === 200) {
                var resp1 = req.response.replace(/Microsoft.Dynamics.CRM.lookuplogicalname/gmi, "logicalname");
                var resp2 = resp1.replace(/Microsoft.Dynamics.CRM.associatednavigationproperty/gmi, "property");
                var resp3 = resp2.replace(/OData.Community.Display.V1.FormattedValue/gmi, "formated"); //_value@Microsoft.Dynamics.CRM.lookuplogicalname
                var result = JSON.parse(resp3);
                return result;
            }
        } catch (e) {
            alert("Retrieve - " + entitySet + ": " + e.message);
        }
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    Create: function (entitySet, entity, callback) {
        /// <summary>This function creates a record using WebAPI with an entity set name, entity object and callback as parameters.</summary>  
        /// <param name="entitySet" type="String">Entity Set Name</param> 
        /// <param name="entity" type="Object">Objekt for creating the entity record</param> 
        /// <param name="callback" type="Object">Function triggered after the data returns - first parameter is GUID of the new record. If this parameter is null, the function is synchronnous.</param> 
        /// <returns type="String">Guid nové entity</returns> 
        try {
            var newEntityId;
            var req = new XMLHttpRequest();
            req.open("POST", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + entitySet, (callback != null));
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState === 4) {
                    req.onreadystatechange = null;
                    if (this.status === 204) {
                        var uri = this.getResponseHeader("OData-EntityId");
                        var regExp = /\(([^)]+)\)/;
                        var matches = regExp.exec(uri);
                        newEntityId = matches[1];
                        if (callback != null) {
                            callback(newEntityId);
                        }
                    } else {
                        Xrm.Utility.alertDialog(this.statusText);
                    }
                }
            };
            req.send(JSON.stringify(entity));
            return newEntityId;
        } catch (e) {
            alert("Create - " + entitySet + ": " + e.message);
        }
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    Update: function (entitySet, id, entity, callback) {
        /// <summary>This function updates a record using WebAPI with an entity set name, entity GUID, entity object and callback as parameters.</summary>  
        /// <param name="entitySet" type="String">Entity Set Name</param> 
        /// <param name="id" type="String">Entity GUID</param> 
        /// <param name="entity" type="Object">Object for updating the entity record</param> 
        /// <param name="callback" type="Object">Function triggered after the data returns. If this parameter is null, the function is synchronnous.</param> 
        /// <returns type="void"></returns> 
        var req = new XMLHttpRequest();
        req.open("PATCH", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + entitySet + "(" + id.replace('{', '').replace('}', '') + ")", (callback != null));
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 204) {
                    if (callback != null) {
                        callback();
                    }
                } else {
                    Xrm.Utility.alertDialog(this.statusText);
                }
            }
        };
        req.send(JSON.stringify(entity));
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A 
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    Delete: function (entitySet, id, callback) {
        /// <summary>This function deletes a record using WebAPI with an entity set name, entity GUID and callback as parameters.</summary>  
        /// <param name="entitySet" type="String">Entity Set Name</param> 
        /// <param name="id" type="String">Entity GUID</param> 
        /// <param name="callback" type="Object">Function triggered after the data returns. If this parameter is null, the function is synchronnous.</param> 
        /// <returns type="void"></returns> 
        var req = new XMLHttpRequest();
        req.open("DELETE", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + entitySet + "(" + id.replace('{', '').replace('}', '') + ")", (callback != null));
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 204) {
                    if (callback != null) {
                        callback();
                    }
                } else {
                    Xrm.Utility.alertDialog(this.statusText);
                }
            }
        };
        req.send();
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    CallAction: function (action, actionParameters) {
        /// <summary>This function calls defined CRM Action using WebAPI with an Action name and Action parameters.</summary>  
        /// <param name="action" type="String">Action name</param> 
        /// <param name="actionParameters" type="String">Action paremeters</param> 
        /// <returns type="void"></returns> 

        var result = null;
        var req = new XMLHttpRequest();
        req.open("POST", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/" + action, false);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 204) {
                    //Success - No Return Data - Do Something
                }
                else if (this.status === 400) {
                    var error = JSON.stringify(JSON.parse(req.response), null, 2);
                    alert("_Update: " + error);
                }
                else if (this.status === 200) {
                    result = JSON.parse(this.response);
                }
            }
        };
        req.send(JSON.stringify(actionParameters));
        return result;
    },

    // Author: Rhino Systems
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    UserHasSpecificRole: function (roleNames, userRoleId) {
        /// <summary>This function verifies if a user has certain role based in a role name.</summary>  
        /// <param name="roleNames" type="String Array">Role Names</param> 
        /// <param name="userRoleId" type="String">User Role Id</param> 
        /// <returns type="void"></returns> 
        try {
            for (var i = 0; i < roleNames.length; i++) {
                var userRole = roleNames[i];
                var object = this.Retrieve('roles', userRoleId, 'name');
                if (object != null) {
                    var roleName = object.results[0].Name;
                    if (userRole == roleName)
                        return true;
                }
            }
            return false;
        }
        catch (e) {
            alert("UserHasSpecificRole: " + e.message);
        }
    }
};

Cemex.Common.UI = {
    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: 
    // Modified On: 
    // Related function(s): N/A
    // Req. ID: N/A
    Info: function (obj) {
        /// <summary>Alerts serialized object to string</summary>  
        /// <param name="obj" type="Object">
        /// </param>  
        /// <returns type="void">ALERT</returns> 
        Xrm.Utility.alertDialog(JSON.stringify(obj, null, 2));
    },

    Log: function (name, obj, severityLevel) {
        /// <summary>Log serializovaného objektu</summary>  
        /// <param name="name" type="String">Name field in Log entity record</param> 
        /// <param name="obj" type="Object">Object to be logged</param>  
        /// <param name="severityLevel" type="int">Severity level of the Log entity record. 0 - Debug, 1 - Info, 2 - Warning, 3 - Error, 4 - Critical</param>  
        /// <returns type="void">ALERT</returns> 
        if (error == null) {
            error = false;
        }
        var entity = {};
        entity.cmx_name = name;
        entity.cmx_severity = severityLevel;
        entity.cmx_formid = Xrm.Page.ui.formSelector.getCurrentItem().getId();
        entity.cmx_formname = Xrm.Page.ui.formSelector.getCurrentItem().getLabel();
        entity.cmx_messagename = "UI";
        entity.cmx_log1 = JSON.stringify(obj, null, 2);
        entity.cmx_primaryentityid = Xrm.Page.data.entity.getId();
        entity.cmx_primaryentityname = Xrm.Page.data.entity.getEntityName();
        ArtJSToolkit.WebAPI.Create("cmx_logs", entity);
    },

    _strRepeat: function (input, multiplier) {
        return new Array(multiplier + 1).join(input);
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A 
    // Related function(s): _strRepeat
    // Req. ID: N/A
    AddZeros: function (number, length) {
        /// <summary>Returns a string with added specified number of zeros to the beginning</summary>  
        /// <param name="number" type="Int">Number to what will the zeros be added</param>  
        /// <param name="length" type="Int">Final length of returned string</param>  
        /// <returns type="string">String with added zeros</returns> 
        if (number.toString().length < length) {
            return _strRepeat("0", length - number.toString().length) + number;
        }
        return number.toString();
    },

    Tabs: {
        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: 
        // Modified On: 
        // Related function(s): N/A
        // Req. ID: N/A
        Get: function (id) {
            /// <summary>Gets Tab by specified ID</summary>  
            /// <param name="id" type="String">Tab ID</param> 
            /// <returns type="object">Tab Object</returns> 
            return Xrm.Page.ui.tabs.get(id);
        },

        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: 
        // Modified On: 
        // Related function(s): N/A
        // Req. ID: N/A
        Collapse: function (id) {
            /// <summary>Collapse Tab by specified ID</summary>  
            /// <param name="id" type="String">Tab ID</param> 
            /// <returns type="void"></returns> 
            var tab = this.Get(id);
            if (tab) {
                tab.setDisplayState("collapsed");
            }
        },

        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: 
        // Modified On: 
        // Related function(s): N/A
        // Req. ID: N/A
        Expand: function (id) {
            /// <summary>Expand Tab by specified ID</summary>  
            /// <param name="id" type="String">Tab ID</param> 
            /// <returns type="void"></returns> 
            var tab = this.Get(id);
            if (tab) {
                tab.setDisplayState("expanded");
            }
        },

        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Hide: function (id) {
            /// <summary>Hide Tab by specified ID</summary>  
            /// <param name="id" type="String">Tab ID</param> 
            /// <returns type="void"></returns> 
            var tab = this.Get(id);
            if (tab) {
                tab.setVisible(false);
            }
        },

        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Show: function (id) {
            /// <summary>Show Tab by specified ID</summary>  
            /// <param name="id" type="String">Tab ID</param> 
            /// <returns type="void"></returns> 
            var tab = this.Get(id);
            if (tab) {
                tab.setVisible(true);
            }
        }
    },

    Fields: {
        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Get: function (fieldName) {
            /// <summary>Get field object by its logical name</summary>  
            /// <param name="fieldName" type="String">Logical name of the field</param> 
            /// <returns type="Object">Field Object</returns> 
            try {
                switch (Xrm.Page.data.entity.attributes.get(fieldName).getAttributeType()) {
                    case 'optionset':
                        return (Xrm.Page.data.entity.attributes.get(fieldName)) ? Xrm.Page.data.entity.attributes.get(fieldName) : null;
                        break;
                    case 'lookup':
                        return (Xrm.Page.data.entity.attributes.get(fieldName)) ? Xrm.Page.data.entity.attributes.get(fieldName).getValue() : null;
                        break;
                    default:
                        return (Xrm.Page.data.entity.attributes.get(fieldName)) ? Xrm.Page.data.entity.attributes.get(fieldName).getValue() : null;
                        break
                }
            } catch (e) {
                alert('Field: ' + fieldName + ' does not exist in current context.')
            }
        },

        // Author: Artemis
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Set: function (fieldName, value) {
            /// <summary>Set field value by its logical name</summary>  
            /// <param name="fieldName" type="String">Logical name of the field</param> 
            /// <param name="value" type="Object, String, Boolean, Numeric...">Value to be set</param> 
            /// <returns type="void"></returns> 
            try {
                Xrm.Page.getAttribute(fieldName).setValue(value);
            } catch (e) {
                alert('Field: ' + fieldName + ' does not exist in current context.')
            }
        },

        // Author: Rhino Systems
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        SetLookup: function (id, name, entityName, fieldName) {
            /// <summary>Set lookup field value by its logical name</summary>  
            /// <param name="id" type="String">GUID reference value</param> 
            /// <param name="name" type="String">Name to show of the related record</param> 
            /// <param name="entityName" type="String">Name of the related entity</param> 
            /// <param name="fieldName" type="String">Logical name of the field</param> 
            /// <returns type="void"></returns> 
            try {
                if (id == null || id == undefined ||
                    name == null || name == undefined ||
                    entityName == null || entityName == undefined)
                    return null;

                var lookupValue = new Array();
                lookupValue[0] = new Object();
                lookupValue[0].id = id;
                lookupValue[0].name = name;
                lookupValue[0].entityType = entityName;

                this.Set(fieldName, lookupValue);
            } catch (e) {
                alert('SetLookup: ' + e.message);
            }
        },

        // Author: Rhino Systems
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        SetRequiredLevel: function (fieldArray, level) {
            /// <summary>Set the type of required level for single or multiple fields in CRM </summary>  
            /// <param name="fieldArray" type="String Array">Array of fields to be modified</param> 
            /// <param name="level" type="String">String that contains de type of required level</param> 
            /// <returns type="void"></returns> 
            var fldArray = [];
            if (fieldArray.constructor !== Array) {
                fldArray[0] = fieldArray;
            } else {
                fldArray = fieldArray;
            }
            for (var i in fldArray) {
                try {
                    var obj = Xrm.Page.data.entity.attributes.get(fldArray[i]);
                    obj.setRequiredLevel(level)
                } catch (e) {
                    alert('Field: ' + fldArray[i] + ' does not exists.');
                }
            }
        },

        // Author: Rhino Systems
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Acces: function (fieldArray, acces) {
            /// <summary>Able or disable single or multiple fields in CRM</summary>  
            /// <param name="fieldArray" type="String Array">Array of fields to be modified</param> 
            /// <param name="acces" type="Boolean">Value to able or disable the field(s)</param> 
            /// <returns type="void"></returns> 
            var fldArray = [];
            if (fieldArray.constructor !== Array) {
                fldArray[0] = fieldArray;
            } else {
                fldArray = fieldArray;
            }
            for (var i in fldArray) {
                try {
                    Xrm.Page.getControl(fldArray[i]).setDisabled(acces);
                } catch (e) {
                    alert('Field: ' + fldArray[i] + ' does not exists.');
                }
            }
        },

        // Author: Rhino Systems
        // Created On: 29-03-2017
        // Modified By: N/A
        // Modified On: N/A
        // Related function(s): N/A
        // Req. ID: N/A
        Visibility: function (fieldArray, visibility) {
            /// <summary>Show or hide single or multiple fields in CRM</summary>  
            /// <param name="fieldArray" type="String Array">Array of fields to be modified</param> 
            /// <param name="visibility" type="Boolean">Value to show or hide the field(s)</param> 
            /// <returns type="void"></returns> 
            var fldArray = [];
            if (fieldArray.constructor !== Array) {
                fldArray[0] = fieldArray;
            } else {
                fldArray = fieldArray;
            }
            for (var i in fldArray) {
                try {
                    Xrm.Page.getControl(fldArray[i]).setVisible(visibility);
                } catch (e) {
                    alert('Field: ' + fldArray[i] + ' does not exists.');
                }
            }
        }
    },

    // Author: Rhino Systems
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    RemoveTextAccents: function (text) {
        /// <summary>This function replaces all accents of vowels. And also it’s  used in CleanSymbolsFromText function.</summary>  
        /// <param name="text" type="String">Text to be replaced</param> 
        /// <returns type="void"></returns> 
        try {
            if (!(text != null && text.length > 0)) return null;
            for (var i = 0; i < text.length; i++) {
                switch (text.charAt(i)) {
                    case "á":
                        text = text.replace(/á/, "a");
                        break;
                    case "é":
                        text = text.replace(/é/, "e");
                        break;
                    case "í":
                        text = text.replace(/í/, "i");
                        break;
                    case "ó":
                        text = text.replace(/ó/, "o");
                        break;
                    case "ú":
                        text = text.replace(/ú/, "u");
                        break;
                    case "Á":
                        text = text.replace(/Á/, "A");
                        break;
                    case "É":
                        text = text.replace(/É/, "E");
                        break;
                    case "Í":
                        text = text.replace(/Í/, "I");
                        break;
                    case "Ó":
                        text = text.replace(/Ó/, "O");
                        break;
                    case "Ú":
                        text = text.replace(/Ú/, "U");
                        break;
                }
            }
            return text;
        }
        catch (ex) {
            alert("Internal error in JS function RemoveTextAccents: " + ex.message);
        }
    },

    // Author: Rhino Systems
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): RemoveTextAccents
    // Req. ID: N/A
    CleanSymbolsFromText: function (executionContext) {
        /// <summary>This function removes all symbols of a string and upper the text.</summary>  
        /// <param name="executionContext" type="Object">Context of the current CRM record</param> 
        /// <returns type="void"></returns> 
        try {
            var attribute = executionContext.getEventSource();
            var fieldName = attribute.getName();
            var fieldToClean = Xrm.Page.getAttribute(fieldName).getValue();
            if (fieldToClean != null) {
                fieldToClean = fieldToClean.replace(/[&\/\|\#-,+()$@~%.ˇ´!'";*ˇż?_>{}]/g, '');
                fieldToClean = this.RemoveTextAccents(fieldToClean);
                if (fieldToClean != null) {
                    fieldToClean = fieldToClean.toUpperCase();
                    Xrm.Page.getAttribute(fieldName).setValue(fieldToClean);
                }
                else
                    Xrm.Page.getAttribute(fieldName).setValue(null);
            }
        }
        catch (ex) {
            alert("Internal error in JS function CleanSymbolsFromText: " + ex.message);

        }
    }
};

Cemex.Common.Metadata = {

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): N/A
    // Req. ID: N/A
    RetrieveEntityMetadata: function (entity, callback) {
        /// <summary></summary>  
        /// <param name="entity" type="Object">Object for retrieve the entity record</param> 
        /// <param name="callback" type="Object">Function triggered after the data returns - first parameter is GUID of the new record. If this parameter is null, the function is synchronnous.</param> 
        /// <returns type="Object"></returns> 
        var req = new XMLHttpRequest();
        req.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/EntityDefinitions?$filter=LogicalName eq '" + entity + "'", true);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var resp = JSON.parse(this.response);
                    callback(resp.value[0]);
                }
                else {
                    alert(this.statusText);
                }
            }
        };
        req.send();
    },

    // Author: Artemis
    // Created On: 29-03-2017
    // Modified By: N/A
    // Modified On: N/A
    // Related function(s): RetrieveEntityMetadata
    // Req. ID: N/A
    RetrieveEntityAttributesMetadata: function (entityName, callback) {
        /// <summary></summary>  
        /// <param name="entity" type="Object">Object for creating the entity record</param> 
        /// <param name="callback" type="Object">Function triggered after the data returns - first parameter is GUID of the new record. If this parameter is null, the function is synchronnous.</param> 
        /// <returns type="Object"></returns> 
        Cemex.Common.Metadata.RetrieveEntityMetadata(entityName, function (entityMetadata) {
            var req = new XMLHttpRequest();
            req.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v" + Cemex.Common.WebAPI.version + "/EntityDefinitions(" + entityMetadata.MetadataId + ")?$select=LogicalName&$expand=Attributes($select=LogicalName,DisplayName,AttributeType)", true);
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState === 4) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var resp = JSON.parse(this.response);
                        callback(resp.Attributes);
                    }
                    else {
                        alert(this.statusText);
                    }
                }
            };
            req.send();
        });
    }
};